#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t' )

oldAuthor = None

scoreList = [-1000000] * 10	# the list of top 10 contributor score (default:-1000000)
authorList = [""] * 10	# the list of top 10 contributor  (default: 10 empty string)
minimumScore = -1000000	# the minimum score in the list
minimumIndex = 0	# the index where the minimum score lies in

authorScore = 0	# current tagFrequency

for line in reader:
	author, score = line
	if oldAuthor and oldAuthor != author:
		if authorScore > minimumScore:
			authorList[ minimumIndex ] = oldAuthor
			scoreList[ minimumIndex ] = authorScore
			# update the minimum frequency in the top 10 list
			minimumScore = min( scoreList )
			# update the index of the minimum frequency 
			minimumIndex = scoreList.index( minimumScore )
			authorScore = 0

	authorScore = authorScore + int(score)
	oldAuthor = author

# sort the score of top 10 author in reversed order and return the indices
sort_index = [ i[0] for i in sorted( enumerate(scoreList), key=lambda x:x[1], reverse = True)]

# output the top 10 tag and corresponding frequency in reverse order
for index in sort_index:
	print "{0}\t{1}".format( authorList[index], scoreList[index])
		
