#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t')
current_node = None
group = []

for line in reader:
	node_id = line[0]
	author_id = line[1]
	if current_node and current_node != node_id:
		str_group = ",".join(group)
		del group[:]
		print "{0}:{1}".format(current_node, str_group)
	
	current_node = node_id
	group.append( str(author_id) )		
