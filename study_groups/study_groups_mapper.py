#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t' )
for line in reader:
	node_type = line[5]
	node_id = line[0]
	author_id = line[3]
	if node_type != "question":
		node_id = line[7]
	print "{0}\t{1}".format(node_id, author_id)

