#!/usr/bin/python
import sys
import csv
from datetime import datetime

reader = csv.reader(sys.stdin, delimiter = '\t')
isFirstLine = True # to skip the header

for line in reader:
	if isFirstLine:
		isFirstLine = False
		continue
	else:
		author_id = line[3]
		added_at =  line[8]
		# to extract the hour from the datetime value
		datetime_added_at = datetime.strptime( added_at[:18], "%Y-%m-%d %H:%M:%S" )
		print "{0}\t{1}".format( author_id, datetime_added_at.hour )
