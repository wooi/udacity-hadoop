#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = "\t" )

for line in reader:
	node_type = line[5]
	if node_type == "answer":
		id = line[7]	# use the parent_id
	else:
		if node_type == "question":
			id = line[0]	# use node id

	# to create a new key by concatenating node id and type with colon (:) as spearator
	key = "{0}:{1}".format(id, node_type)
	value = len(line[4])	# length of 'body'
	print "{0}\t{1}".format(key, value)

