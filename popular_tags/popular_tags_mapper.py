#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t' )

for line in reader:
	tags = line[2].split()
	for tag in tags:
		print "{0}\t1".format(tag)
