#!/usr/bin/python
import sys

oldKey = None
node_answer = []
node_question = []

for line in sys.stdin:
	key, value = line.split('\t')
	id, node_type = key.split(':')
	
	if oldKey and oldKey != id:

		# to output the question and average answer length
		# use max function as denomiator to avoid division by zero in the case of no answer.
		average_answer = sum( node_answer )/ max(1,len(node_answer))
		
		# assume that there are multiple questions; len(node_question) should be always 1
		average_question = sum( node_question )/max(1,len(node_question))
		print "{0}\t{1}\t{2}".format( id, average_question, average_answer )

		# reset
		del node_answer[:]
		del node_question[:]

	oldKey = id
	if node_type == "answer":
		node_answer.append( int(value) )
	else:	# confirm that if is not a answer, then is a question
		node_question.append( int(value) )			
