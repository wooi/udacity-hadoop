#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t' )

oldTag = None

frequencyList = [0] * 10	# the list of top ten tag frequency (default: 0)
tagList = [""] * 10	# the list of top ten tag (default: 10 empty string)
minimumFrequency = 0	# the minimum frequency in the list
minimumIndex = 0	# the index where the minimum frequency lies in

tagFrequency = 0	# current tagFrequency

for line in reader:
	tag, frequency = line
	if oldTag and oldTag != tag:
		if tagFrequency > minimumFrequency:
			tagList[ minimumIndex ] = oldTag
			frequencyList[ minimumIndex ] = tagFrequency
			# update the minimum frequency in the top 10 list
			minimumFrequency = min( frequencyList )
			# update the index of the minimum frequency 
			minimumIndex = frequencyList.index( minimumFrequency )
			tagFrequency = 0

	tagFrequency = tagFrequency + int(frequency)
	oldTag = tag

# sort the frequency of top 10 tags in reversed order and return the indices
sort_index = [ i[0] for i in sorted( enumerate(frequencyList), key=lambda x:x[1], reverse = True)]

# output the top 10 tag and corresponding frequency in reverse order
for index in sort_index:
	print "{0}\t{1}".format( tagList[index], frequencyList[index])
		
