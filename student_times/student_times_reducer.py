#!/usr/bin/python
import sys

# to return a list of 24 elements corresponding to the total hit in each hour
def getHourlyHit():
	l = [0] * 24
	return l

hourlyHit = getHourlyHit()
oldKey = None

for line in sys.stdin:
	thisKey,  hour = line.strip().split("\t")

	if oldKey and oldKey != thisKey:
		# select the hour with maximum hit
		max_frequency = max( hourlyHit )
		hours_max_frequency = [i for i,v in enumerate(hourlyHit) if v == max_frequency] # the hour (0-23hr) has one or more max hit (in case of tie).
		for hour in hours_max_frequency:
			print "{0}\t{1}".format(thisKey, hour)
		hourlyHit = getHourlyHit() # reset the hit

	oldKey = thisKey
	int_hour = int( hour )
	hourlyHit[ int_hour ] = hourlyHit[ int_hour ] + 1
