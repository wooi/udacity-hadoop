#!/usr/bin/python
import sys
import csv

reader = csv.reader( sys.stdin, delimiter = '\t' )

for line in reader:
	author_id = line[3]
	node_type = line[5]
	score = line[9]
	if node_type == "answer":
		print "{0}\t{1}".format(author_id, score)
